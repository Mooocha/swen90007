// app/middleware/authMid.js

// use strict mode
"use strict"

// ===============================
// call packages
// ===============================
var express = require("express")
var jwt = require("jsonwebtoken")
var jwtConfig = require("./../../config/jwtConfig")
var authExlude = require('./../../config/MidExclude').auth
var async = require('async')

// ===============================
// call models
// ===============================
var Seller = require("./../model/seller")
var Customer = require('../model/customer')

// define router
var authMid = express.Router()

var isExclude = function(req) {
    var result = false
    authExlude.forEach(function(ele) {
        if (ele.path == req.path && ele.method == req.method) {
            result = true
        }
    })
    return result
}

authMid.use(function(req, res, next) {
    if (isExclude(req)) {
        next()
    } else {
        if (!req.header("Authorization")) {
            res.status(403)
                .send({
                    "msg": "Authentication credentials were not provided!"
                })
        } else {
            var token = req.header("Authorization")
            jwt.verify(token, jwtConfig.key,
                function(err, decoded) {
                    if (err) {
                        console.log(err)
                        if (err.name == "TokenExpiredError") {
                            res.status(403)
                                .send({
                                    "err": err
                                })
                        }
                        res.status(500)
                            .send({
                                "err": err
                            })
                    }

                    if (decoded) {
                        async.parallel([
                            function(cb) {
                                Seller.findOne({
                                    "email": decoded.email,
                                    "password": decoded.password
                                }, function(err, seller) {
                                    if (err) {
                                        console.log(err)
                                        res.status(500)
                                            .send({
                                                "msg": "Internal Error!"
                                            })
                                    }
                                    if (seller) {
                                        seller.role = 'seller'
                                    }
                                    cb(null, seller)
                                })
                            },
                            function(cb) {
                                Customer.findOne({
                                    email: decoded.email,
                                    password: decoded.password
                                }, function(err, customer) {
                                    if (err) {
                                        console.log(err)
                                        res.status(500)
                                            .send({
                                                msg: 'Internal Error!'
                                            })
                                    }
                                    if (customer) {
                                        customer.role = 'customer'
                                    }
                                    cb(null, customer)
                                })
                            }
                        ], function(err, results) {
                            var user = results[0] || results[1]

                            if (user) {
                                req.user = user
                                next()
                            } else {
                                res.status(403)
                                    .send({
                                        "msg": "Invalid Token!"
                                    })
                            }
                        })
                    } else {
                        res.status(403)
                            .send({
                                "msg": "Invalid Token!"
                            })
                    }
                })
        }
    }
})


module.exports = authMid
