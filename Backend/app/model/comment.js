/**
 * backend/app/model/comment.js
 */

'use strict'

// ============================
// call packages
// ============================
var mongoose = require("mongoose")

// ============================
// schema
// ============================
var commentSchema = new mongoose.Schema({

    customerId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: "Customer",
    },

    productId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: "Product",
    },

    date: {
        type: 'Date',
        default: Date.now,
    },

    content: {
        type: 'String',
        default: '',
    },

})

module.exports = mongoose.model("Comment", commentSchema)
