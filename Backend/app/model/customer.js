/**
 * backend/app/model/customer.js
 */

'use strict'

// ============================
// call packages
// ============================
var mongoose = require("mongoose")

// ============================
// schema
// ============================
var customerSchema = new mongoose.Schema({
    email: {
        type: 'String',
        required: true
    },

    password: {
        type: 'String',
        required: true
    },

    firstName: {
        type: 'String',
        required: true
    },

    lastName: {
        type: 'String',
        required: true
    },

    mobile: {
        type: 'String',
        default: ''
    },

    address: {
        type: 'String',
        default: ''
    },

    points: {
        type: 'Number',
        default: 0
    }
})

module.exports = mongoose.model("Customer", customerSchema)
