/**
 * backend/app/model/order.js
 */

'use strict'

// ============================
// call packages
// ============================
var mongoose = require("mongoose")
var moment = require('moment')

// ============================
// schema
// ============================
var orderSchema = new mongoose.Schema({

    products: [{
        product: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: "Product",
        },
        amount: {
            type: 'Number',
            default: 0
        }
    }],

    customerId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Customer'
    },

    sellerId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Customer'
    },

    status: {
        type: 'String',
        default: '',
    },

    orderTime: {
        type: 'Date',
        default: Date.now,
    },

    deliverTime: {
        type: 'Date',
        required: true
    },

    price: {
        type: 'Number',
        default: 0
    }
})

orderSchema.methods.isToDelete = function() {
    var deliverTime = moment(this.deliverTime)
    if (moment().add(1, 'days').isAfter(deliverTime)) {
        return false
    } else {
        return true
    }
}

module.exports = mongoose.model("Order", orderSchema)
