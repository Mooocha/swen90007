/**
 * backend/app/model/product.js
 */

'use strict'

// ============================
// call packages
// ============================
var mongoose = require("mongoose")

// ============================
// schema
// ============================
var productSchema = new mongoose.Schema({

    sellerId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: "Seller",
    },

    price: {
        type: 'Number',
        required: true
    },

    name: {
        type: 'String',
        required: true
    },

    rating: {
        type: 'Number',
        default: 0
    },

    imageUrl: {
        type: 'String',
        default: ''
    },

    category: {
        type: 'String',
        default: 'default'
    },

    status: {
        type: 'String',
        default: '',
        enum: ['new', 'sale', 'soldOut', '']
    }
})


module.exports = mongoose.model("Product", productSchema)
