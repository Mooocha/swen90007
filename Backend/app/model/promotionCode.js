/**
 * backend/app/model/promotionCode.js
 */

'use strict'

// ============================
// call packages
// ============================
var mongoose = require("mongoose")

// ============================
// schema
// ============================
var promotionCode = new mongoose.Schema({
    seller: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Seller'
    },

    discount: {
        type: 'Number',
        required: true,
    },

    startDate: {
        type: 'Date',
        required: true
    },

    endDate: {
        type: 'Date',
        required: true
    }
})

module.exports = mongoose.model("promotionCode", promotionCode)
