/**
 * backend/app/model/qrcode.js
 */

'use strict'

// ============================
// call packages
// ============================
var mongoose = require("mongoose")

// ============================
// schema
// ============================
var qrcodeSchema = new mongoose.Schema({
    content: {
        type: 'String',
        required: true
    }
})

module.exports = mongoose.model("Qrcode", qrcodeSchema)
