/**
 * backend/app/model/seller.js
 */

'use strict'

// ============================
// call packages
// ============================
var mongoose = require("mongoose")

// ============================
// schema
// ============================
var sellerSchema = new mongoose.Schema({
    email: {
        type: 'String',
        required: true
    },

    password: {
        type: 'String',
        required: true
    },

    firstName: {
        type: 'String',
        required: true
    },

    lastName: {
        type: 'String',
        required: true
    },

    mobile: {
        type: 'String',
        default: ''
    },

    address: {
        type: 'String',
        default: ''
    }
})

module.exports = mongoose.model("Seller", sellerSchema)
