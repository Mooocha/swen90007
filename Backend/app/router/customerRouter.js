/**
 * Backend/app/router/userRouter.js
 */

'use strict'

// =====================================
// call packages
// =====================================
var express = require("express")
var async = require('async')

// =====================================
// call models
// =====================================
var Customer = require('./../model/customer')

var customerRouter = express.Router()

customerRouter
    .route('/customers')
    .post(function(req, res) {
        async.series([
            function(cb) {
                Customer.findOne({
                    email: req.body.email
                }, function(err, customer) {
                    if (err) {
                        console.log(err)
                        res.status(500)
                            .send({
                                msg: err
                            })
                    }

                    if (customer) {
                        res.status(400)
                            .send({
                                msg: 'User exist'
                            })
                    } else {
                        cb(null)
                    }
                })
            },
            function(cb) {
                var customer = new Customer(req.body)
                customer.save(function(err) {
                    if (err) {
                        console.log(err)
                        res.status(500)
                            .send({
                                msg: err
                            })
                    }
                    res.status(201)
                        .send({
                            customer: customer
                        })

                })
            }
        ])
    })
    .get(function(req, res) {
        Customer.find(req.query, function(err, customers) {
            if (err) {
                console.log(err)
                res.status(500)
                    .send({
                        msg: err
                    })
            }
            res.send({
                customers: customers
            })
        })
    })

customerRouter
    .route('/customer/:customerId')
    .get(function(req, res) {
        Customer.findOne({
            '_id': req.params.customerId
        }, function(err, customer) {
            if (err) {
                console.log(err)
                res.status(500)
                    .send({
                        msg: err
                    })
            }
            if (customer) {
                res.send({
                    customer: customer
                })
            } else {
                res.status(404)
                    .send({
                        msg: 'Not Found'
                    })
            }
        })
    })
    .put(function(req, res) {
        async.waterfall([
            function(cb) {
                Customer.findOne({
                    '_id': req.params.customerId
                }, function(err, customer) {
                    if (err) {
                        console.log(err)
                        res.status(500)
                            .send({
                                msg: err
                            })
                    }

                    if (customer) {
                        cb(null, customer)
                    } else {
                        res.status(404)
                            .send({
                                msg: 'Not Found'
                            })
                    }
                })
            },
            function(customer, cb) {
                for (var key in req.body) {
                    customer[key] = req.body[key]
                }
                customer.save(function(err) {
                    if (err) {
                        console.log(err)
                        res.status(500)
                            .send({
                                msg: err
                            })
                    }
                    res.send({
                        customer: customer
                    })
                })
            }
        ])

    })

module.exports = customerRouter
