'use strict'

/**
 * call packages
 */
var express = require('express')
var async = require('async')

/**
 * call models
 */
var Order = require('../model/order')

var orderRouter = express.Router()

orderRouter
    .route('/orders')
    .get(function(req, res) {
        var queryObj = {}
        if (req.user.role == 'customer') {
            queryObj.customerId = req.user._id
        } else if (req.user.role == 'seller') {
            queryObj.sellerId = req.user._id
        }

        Order.find(queryObj, function(err, orders) {
            if (err) {
                console.log(err)
                res.status(500)
                    .send({
                        msg: 'Internal Error!'
                    })
            }

            res.send({
                orders: orders
            })
        })
    })
    .post(function(req, res) {
        var order = new Order(req.body)
        order.save(function(err) {
            if (err) {
                console.log(err)
                res.status(500)
                    .send({
                        msg: 'Internal Error!'
                    })
            }

            res.send({
                order: order
            })
        })
    })

orderRouter
    .route('/order/:id')
    .get(function(req, res) {
        Order.findOne({
            _id: req.params.id
        }, function(err, order) {
            if (err) {
                res.status(500)
                    .send({
                        "error": "Internal Error!"
                    })
            } else {
                res.send({
                    order: order
                })
            }
        })
    })
    .put(function(req, res) {
        Order.findOne({
            _id: req.params.id
        }, function(err, order) {
            if (err) {
                res.status(500)
                    .send({
                        "error": "Internal Error!"
                    })
            } else {
                if (order) {
                    if (order.isToDelete()) {
                        order.status = req.body.status
                        order.save(function(err, order) {
                            if (err) {
                                res.status(500)
                                    .send({
                                        "error": "Internal Error!"
                                    })
                            } else {
                                res.send({
                                    order: order
                                })
                            }
                        })
                    } else {
                        res.status(400)
                            .send({
                                msg: 'Time less than 1 day!'
                            })
                    }
                } else {
                    res.status(404)
                        .send({
                            'msg': 'Not Found!'
                        })
                }
            }
        })
    })
    .delete(function(req, res) {
        async.series([
            function(cb) {
                Order.findOne({
                    _id: req.params.id
                }, function(err, order) {
                    if (err) {
                        res.status(500)
                            .send({
                                "error": "Internal Error!"
                            })
                    }

                    if (order) {
                        if (order.isToDelete()) {
                            cb(null)
                        } else {
                            res.status(400)
                                .send({
                                    msg: 'Time less than 1 day!'
                                })
                        }
                    } else {
                        res.status(404)
                            .send({
                                msg: 'Not Found!'
                            })
                    }
                })
            }
        ], function(cb) {
            Order.remove({
                _id: req.params.id
            })
        })
    })

module.exports = orderRouter
