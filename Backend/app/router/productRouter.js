"use strict"

var express = require("express")

var Product = require("../model/product")

var productRouter = express.Router()

productRouter
//get all products data
    .route("/products")
    .get(function(req, res) {
        var sellerId = ''
        if (req.user.role == 'seller') {
            sellerId = req.user._id
        } else {
            sellerId = req.query.sellerId
        }

        Product.find({
            "sellerId": sellerId
        }, function(err, products) {
            if (err) {
                res.status(500)
                    .send({
                        "error": "Internal Error!"
                    })
            } else {
                res.send({
                    "products": products
                })
            }
        })
    })
    //create new product
    .post(function(req, res) {
        Product.findOne({
            "sellerId": req.body.sellerId,
            "name": req.body.name
        }, function(err, product) {
            if (err) {
                res.status(500)
                    .send({
                        "error": "Internal Error!"
                    })
            } else {
                if (product) {
                    res.send({
                        "msg": "Product name exits!"
                    })
                } else {
                    var product = new Product({
                        "sellerId": req.user._id,
                        "name": req.body.name,
                        "price": req.body.price,
                        "rating": req.body.rating,
                        "imageUrl": req.body.imageUrl,
                        "status": req.body.state,
                        "category": req.body.category,
                    })
                    product.save(function(err) {
                        if (err) {
                            res.status(500)
                                .send({
                                    "error": "Internal Error!"
                                })
                        } else {
                            res.send({
                                "msg": "Successfully saved."
                            })
                        }
                    })
                }
            }
        })
    })

productRouter
//get single product
    .route("/product/:id")
    .get(function(req, res) {
        Product.findOne({
            _id: req.params.id
        }, function(err, product) {
            if (err) {
                res.status(500)
                    .send({
                        "error": "Internal Error!"
                    })
            } else {
                res.send({
                    "product": product
                })
            }
        })
    })
    //edit product
    .put(function(req, res) {
        Product.findOne({
            _id: req.params.id
        }, function(err, product) {
            if (err) {
                res.send({
                    "error": "Internal Error!"
                })
            } else {
                if (product) {
                    product.name = req.body.name
                    product.price = req.body.price
                    product.imageUrl = req.body.imageUrl
                    product.category = req.body.category
                    product.save(function(err, product) {
                        if (err) {
                            res.status(500)
                                .send({
                                    "error": "Internal Error!"
                                })
                        } else {
                            res.send({
                                "product": product
                            })
                        }
                    })
                }
            }
        })
    })
    //delete product
    .delete(function(req, res) {
        Product.remove({
            _id: req.params.id
        }, function(err) {
            if (err) {
                res.status(500)
                    .send({
                        "error": "Internal Error!"
                    })
            } else {
                res.send({
                    "msg": "Successfully deleted!"
                })
            }
        })
    })

module.exports = productRouter
