/**
 * Backend/app/router/sellerRouter.js
 */

'use strict'

// =====================================
// call packages
// =====================================
var express = require("express")
var async = require('async')

// =====================================
// call models
// =====================================
var Seller = require('./../model/seller')

var sellerRouter = express.Router()

sellerRouter
    .route('/sellers')
    .post(function(req, res) {
        async.series([
            function(cb) {
                Seller.findOne({
                    email: req.body.email
                }, function(err, seller) {
                    if (err) {
                        console.log(err)
                        res.status(500)
                            .send({
                                msg: err
                            })
                    }

                    if (seller) {
                        res.status(400)
                            .send({
                                msg: 'User exist'
                            })
                    } else {
                        cb(null)
                    }
                })
            },
            function(cb) {
                var seller = new Seller(req.body)
                seller.save(function(err) {
                    if (err) {
                        console.log(err)
                        res.status(500)
                            .send({
                                msg: err
                            })
                    }
                    res.status(201)
                        .send({
                            seller: seller
                        })
                })
            }
        ])
    })
    .get(function(req, res) {
        Seller.find(req.query, function(err, sellers) {
            if (err) {
                console.log(err)
                res.status(500)
                    .send({
                        msg: err
                    })
            }
            res.send({
                sellers: sellers
            })
        })
    })

sellerRouter
    .route('/seller/:sellerId')
    .get(function(req, res) {
        Seller.findOne({
            '_id': req.params.sellerId
        }, function(err, seller) {
            if (err) {
                console.log(err)
                res.status(500)
                    .send({
                        msg: err
                    })
            }
            if (seller) {
                res.send({
                    seller: seller
                })
            } else {
                res.status(404)
                    .send({
                        msg: 'Not Found'
                    })
            }
        })
    })
    .put(function(req, res) {
        async.waterfall([
            function(cb) {
                Seller.findOne({
                    '_id': req.params.sellerId
                }, function(err, seller) {
                    if (err) {
                        console.log(err)
                        res.status(500)
                            .send({
                                msg: err
                            })
                    }

                    if (seller) {
                        cb(null, seller)
                    } else {
                        res.status(404)
                            .send({
                                msg: 'Not Found'
                            })
                    }
                })
            },
            function(seller, cb) {
                for (var key in req.body) {
                    seller[key] = req.body[key]
                }
                seller.save(function(err) {
                    if (err) {
                        console.log(err)
                        res.status(500)
                            .send({
                                msg: err
                            })
                    }
                    res.send({
                        seller: seller
                    })
                })

            }
        ])

    })

module.exports = sellerRouter
