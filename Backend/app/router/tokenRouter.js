/**
 * Backend/app/router/tokenRouter.js
 */

'use strict'

// =============================
// call packages 
// =============================
var express = require("express")
var jwt = require("jsonwebtoken")
var jwtConfig = require("./../../config/jwtConfig")
var async = require('async')

// =============================
// call models
// =============================
var Customer = require("./../model/customer")
var Seller = require('./../model/seller')

// define router
var tokenRouter = express.Router()

tokenRouter
    .route("/tokens")
    .post(function(req, res) {
        console.log(req.body)
        async.parallel([
            function(cb) {
                Customer.findOne({
                    email: req.body.email,
                    password: req.body.password
                }, function(err, customer) {
                    if (err) {
                        cb(err)
                    }
                    cb(null, customer)
                })
            },
            function(cb) {
                Seller.findOne({
                    email: req.body.email,
                    password: req.body.password
                }, function(err, seller) {
                    if (err) {
                        cb(err)
                    }
                    cb(null, seller)
                })
            }
        ], function(err, results) {
            if (err) {
                console.log(err)
                res.status(500)
                    .send({
                        msg: err
                    })
            }

            var user = results[0] || results[1]

            if (user) {
                var role
                if (results[0]) {
                    role = 'customer'
                } else if (results[1]) {
                    role = 'seller'
                }

                var token = jwt.sign({
                    "email": user.email,
                    "password": user.password
                }, jwtConfig.key)

                res.send({
                    "id": user.id,
                    "token": token,
                    "firstName": user.firstName,
                    role: role
                })
            } else {
                res.status(403)
                    .send({
                        msg: 'Authentication fails'
                    })
            }
        })
    })

module.exports = tokenRouter
