"use strict"

var express = require("express")

var uploadRouter = express.Router()

var multipart = require('connect-multiparty')

var multipartMiddleware = multipart()

var fs = require("fs")

uploadRouter
    .route("/uploads/:id")
    //upload image
    .post(multipartMiddleware, function(req, res) {
        var file = req.files.file
        console.log(file)
        fs.readFile(file, function(err, data) {
            var newPath = "/uploads/uploadedFileName";
            fs.writeFile(newPath, data, function(err) {
                //res.redirect("back");
                res.send({
                    "msg": 112
                })
            });
        });

        // Product.findOne({
        //     "sellerId": req.body.sellerId,
        //     "name": req.body.name
        // }, function(err, product) {
        //     if (err) {
        //         res.status(500)
        //             .send({
        //                 "error": "Internal Error!"
        //             })
        //     } else {
        //         if (product) {
        //             res.send({
        //                 "msg": "Product name exits!"
        //             })
        //         } else {
        //             var product = new Product({
        //                 "sellerId": req.body.sellerId,
        //                 "name": req.body.name,
        //                 "price": req.body.price,
        //                 "rating": req.body.rating,
        //                 "imageUrl": req.body.imageUrl,
        //                 "state": req.body.state,
        //                 "category": req.body.category,
        //             })
        //             product.save(function(err) {
        //                 if (err) {
        //                     res.status(500)
        //                         .send({
        //                             "error": "Internal Error!"
        //                         })
        //                 } else {
        //                     res.send({
        //                         "msg": "Successfully saved."
        //                     })
        //                 }
        //             })
        //         }
        //     }
        // })
    })

module.exports = uploadRouter