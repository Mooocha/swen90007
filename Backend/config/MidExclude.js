'use strict'

module.exports = {
    auth: [{
        path: '/tokens',
        method: 'POST'
    }, {
        path: '/sellers',
        method: 'POST'
    }, {
    	path: '/customers',
    	method: 'POST'
    }]
}
