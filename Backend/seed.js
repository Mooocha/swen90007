/**
 * backend/seed.js
 */

'use strict'

var db = require("./config/dbConfig")

// ==================================
// connect to database
// ==================================
db.connect()

var Seller = require('./app/model/seller')
var Product = require('./app/model/product')
var Customer = require('./app/model/customer')

var sellers = []
sellers.push(new Seller({
    email: 'seller1@90007.com',
    password: '123456',
    firstName: 'f1',
    lastName: 'l1',
    mobile: '001',
    address: 'central'
}))

sellers.push(new Seller({
    email: 'seller2@90007.com',
    password: '123456',
    firstName: 'f2',
    lastName: 'l2',
    mobile: '002',
    address: 'southbank'
}))

sellers.push(new Seller({
    email: 'seller3@90007.com',
    password: '123456',
    firstName: 'f3',
    lastName: 'l3',
    mobile: '003',
    address: 'bruswick'
}))

Seller.remove({}, function(err) {
    sellers.forEach(function(seller) {
        seller.save()
    })
})

var products = []
products.push(new Product({
    sellerId: sellers[0]._id,
    "name": 'kfc',
    "price": 10
}))


products.push(new Product({
    sellerId: sellers[0]._id,
    "name": 'mc',
    "price": 20
}))


products.push(new Product({
    sellerId: sellers[1]._id,
    "name": 'pizzahut',
    "price": 30
}))



Product.remove({}, function(err) {
    products.forEach(function(product) {
        product.save()
    })
})


var customer = new Customer({
    email: 'customer1@90007.com',
    password: '123456',
    firstName: 'customer1',
    lastName: 'customer1'
})

Customer.remove({}, function(err) {
    customer.save()
})
