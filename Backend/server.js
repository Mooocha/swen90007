/**
 * backend/server.js
 */

// use strict mode
"use strict"

// ==================================
// BASE SETUP
// ==================================
var app = require("./config/serverConfig").app
var PORT = require("./config/serverConfig").port
var db = require("./config/dbConfig")

// ==================================
// connect to database
// ==================================
db.connect()

// ==================================
// load middleware
// ==================================
var authMid = require('./app/middleware/authMid')



// ==================================
// load routers
// ==================================
var productRouter = require("./app/router/productRouter")
var tokenRouter = require('./app/router/tokenRouter')
var customerRouter = require('./app/router/customerRouter')
var sellerRouter = require('./app/router/sellerRouter')
var uploadRouter = require('./app/router/uploadRouter')
var orderRouter = require('./app/router/orderRouter')

// ==================================
// apply middlewares
// ==================================
app.use('/api', authMid)

// ==================================
// apply routers
// ==================================
app.use('/api', tokenRouter)
app.use('/api', customerRouter)
app.use('/api', sellerRouter)
    //product router
app.use("/api", productRouter)
    //upload router
app.use("/api", uploadRouter)
app.use('/api', orderRouter)


// test path
app.get("/test", function(req, res) {
    res.send({
        "msg": "hehehe"
    })
})

app.get("/api/test", function(req, res) {
    res.send({
        "msg": "authentication succeeds!"
    })
})

app.listen(PORT, function() {
    console.log("Listen to port {}".format(PORT))
})
