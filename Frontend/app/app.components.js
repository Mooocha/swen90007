"use strict"

angular
	.module("app.components", [
		"app.components.nav",
		"app.components.seller",
		"app.components.shared",
		"app.components.product",
		"app.components.customer"
		])