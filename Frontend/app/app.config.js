"use strict"

angular
    .module("app.config", [
        "ui.router",
        "ngFileUpload",
    ])
    .constant('ENV', 'development')
    .config(['$httpProvider',
        function($httpProvider) {
            $httpProvider.interceptors.push("authInterceptor")
        }
    ])
