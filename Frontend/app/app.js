"use strict"

angular
    .module("frontend", [
    	"app.router", 
    	"app.components", 
    	"app.restapi",
    	"app.config",
    	"app.shared"
    	])