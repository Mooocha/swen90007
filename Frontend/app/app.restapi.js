"use strict"

angular
    .module("app.restapi", [])
    .constant("HOST", "http://localhost:8080/api")
    .factory("restapiUrl", ["HOST", "$http",
        function(HOST, $http) {
            return {
                customers: function() {
                    return HOST + "/customers"
                },
                sellers: function() {
                    return HOST + "/sellers"
                },
                tokens: function() {
                    return HOST + "/tokens"
                },
                products: function() {
                    return HOST + "/products"
                },

                product: function(params) {
                    return HOST + '/product/' + params._id
                },

                seller: function(id) {
                    return HOST + "/seller/" + id
                },

                orders: function() {
                    return HOST + "/orders"
                },

                order: function(id) {
                    return HOST + "/order/" + id
                }
            }
        }
    ])

.factory("restapi", ["restapiUrl", "$http",
    function(restapiUrl, $http) {
        return {
            "customers": {
                register: function(data) {
                    return $http.post(restapiUrl.customers(), data)
                },
            },
            "sellers": {
                register: function(data) {
                    return $http.post(restapiUrl.sellers(), data)
                },
                get: function() {
                    return $http.get(restapiUrl.sellers())
                }
            },
            "seller": {
                get: function(id) {
                    return $http.get(restapiUrl.seller(id))
                }
            },
            "tokens": {
                login: function(data) {
                    return $http.post(restapiUrl.tokens(), data)
                },
            },
            "products": {
                create: function(data) {
                    return $http.post(restapiUrl.products(), data)
                },

                get: function(params) {
                    return $http.get(restapiUrl.products(), {
                        params: params
                    })
                }
            },
            'product': {
                get: function(params) {
                    return $http.get(restapiUrl.product(params))
                },

                put: function(params) {
                    return $http.put(restapiUrl.product(params), params)
                },

                delete: function(params) {
                    return $http.delete(restapiUrl.product(params))
                }

            },
            "orders": {
                get: function() {
                    return $http.get(restapiUrl.orders())
                },

                post: function(params) {
                    return $http.post(restapiUrl.orders(), params)
                }
            },
            "order": {
                get: function(id) {
                    return $http.get(restapiUrl.order(id))
                },

                put: function(id, data) {
                    return $http.put(restapiUrl.order(id), data)
                }
            }
        }
    }
])
