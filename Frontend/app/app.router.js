"use strict"

angular
    .module("app.router", [
        "ui.router"
    ])
    .config(["$stateProvider",
        function($stateProvider) {
            $stateProvider
                .state("app", {
                    "abstract": true,
                    "views": {
                        "nav": {
                            "templateUrl": "app/components/nav/nav.html",
                            "controller": "navCtrl"
                        },
                        "footer": {
                            "templateUrl": "app/components/footer/footer.html"
                        },
                    }
                })

            //shared
            .state("app.shared", {
                    "abstract": true,
                    "views": {
                        "container@": {
                            "templateUrl": "app/components/shared/shared.html",
                        }
                    }
                })
                //login and register
                .state("app.shared.loginAndRegister", {
                    "url": "/loginAndRegister",
                    "views": {
                        "sharedContainer": {
                            "templateUrl": "app/components/shared/loginAndRegister/loginAndRegister.html",
                            "controller": "loginAndRegisterCtrl"
                        }
                    }
                })

            //customer
            .state("app.customer", {
                "abstract": true,
                "views": {
                    "slider@": {
                        "templateUrl": "app/components/slider/slider.html"
                    },
                    "container@": {
                        "templateUrl": "app/components/customer/customer.html",
                    }
                }
            })

            //index page
            .state("app.customer.index", {
                    "url": "/",
                    "views": {
                        "customerContainer": {
                            "templateUrl": "app/components/customer/index/index.html",
                            "controller": "customerIndexCtrl"
                        },
                        'nav@': {
                            "templateUrl": "app/components/nav/nav.html",
                            "controller": "navCtrl"
                        }
                    }
                })
                .state('app.customer.orders', {
                    url: '/customer/orders',
                    'views': {
                        customerContainer: {
                            templateUrl: 'app/components/seller/order/order.html',
                            controller: 'sellerOrderCtrl'
                        },
                        "slider@": {
                            "templateUrl": ""
                        },
                    }
                })

            //seller
            .state("app.seller", {
                    "abstract": true,
                    "url": "/seller",
                    "views": {
                        // "slider@": {
                        //     "templateUrl": "app/components/slider/slider.html"
                        // },
                        "container@": {
                            "templateUrl": "app/components/seller/seller.html",
                        }
                    }
                })
                //index page
                .state("app.seller.index", {
                    "url": "/",
                    "views": {
                        "sellerContainer": {
                            "templateUrl": "app/components/seller/index/index.html",
                            "controller": "sellerIndexCtrl"
                        }
                    }
                })
                //order page
                .state("app.seller.orders", {
                    "url": "/orders",
                    "views": {
                        "sellerContainer": {
                            "templateUrl": "app/components/seller/order/order.html",
                            "controller": "sellerOrderCtrl"
                        }
                    }
                })
                //manage page
                .state("app.seller.manage", {
                    "url": "/manage/:id",
                    "views": {
                        "sellerContainer": {
                            "templateUrl": "app/components/seller/manage/manage.html",
                            "controller": "sellerManageCtrl"
                        }
                    }
                })

            //product 
            .state("app.products", {
                    "abstract": true,
                    "url": "/products",
                    "views": {
                        "container@": {
                            "templateUrl": "app/components/product/product.html",
                        }
                    }
                })
                //new product page
                .state("app.products.new", {
                    "url": "/new",
                    "views": {
                        "productContainer": {
                            "templateUrl": "app/components/product/new/new.html",
                            "controller": "productNewCtrl"
                        }
                    }
                })
                //edit product page
                .state("app.products.edit", {
                    "url": "/edit/:id",
                    "views": {
                        "productContainer": {
                            "templateUrl": "app/components/product/edit/edit.html",
                            "controller": "productEditCtrl"
                        }
                    }
                })
                //product list page
                .state("app.products.list", {
                    "url": "/list/:id",
                    "views": {
                        "productContainer": {
                            "templateUrl": "app/components/product/list/list.html",
                            "controller": "productListCtrl"
                        }
                    }
                })


        }
    ])

.config(["$urlRouterProvider",
    function($urlRouterProvider) {
        //if no routes match, direct the user to this url
        $urlRouterProvider.otherwise("/seller/")
    }
])
