//shared method
"use strict";

angular
    .module("app.shared", [])
    .factory("user", ["$window",
        function($window) {
            return {
                get: function() {
                    return {
                        "role": $window.localStorage.getItem("role"),
                        "firstName": $window.localStorage.getItem("firstName"),
                        token: $window.localStorage.getItem('token'),
                        id: $window.localStorage.getItem('id')
                    };
                },

                set: function(id, role, firstName, token) {
                    $window.localStorage.setItem("role", role);
                    $window.localStorage.setItem("firstName", firstName);
                    $window.localStorage.setItem('token', token)
                    $window.localStorage.setItem('id', id)
                },

                remove: function() {
                    $window.localStorage.removeItem("role");
                    $window.localStorage.removeItem("firstName");
                    $window.localStorage.removeItem('token')
                    $window.localStorage.removeItem('id')
                },

                isLogin: function() {
                    if ($window.localStorage.getItem('token') && $window.localStorage.getItem('firstName')) {
                        return true
                    } else {
                        return false
                    }
                }
            };
        }
    ])
    .factory("authInterceptor", ["$injector", "user",
        function($injector, user) {
            return {
                request: function(config) {
                    var jwtToken = user.get().token
                    if (jwtToken) {
                        config.headers.authorization = jwtToken
                    }
                    return config
                },

                responseError: function(res) {
                    var $state = $injector.get("$state")
                    if (res.status == 403) {
                        if (user.get().token) {
                            user.remove()
                            alert("Session Time Out")
                        }
                        $state.go("app.shared.loginAndRegister")
                    }
                    return res
                }
            }
        }
    ])
    .factory('categories', [
        function() {
            return function(products) {
                var categories = []
                for (var i = 0; i < products.length; i++) {
                    if (categories.indexOf(products[i].category) < 0) {
                        categories.push(products[i].category)
                    }
                }
                return categories
            }
        }
    ])
    .factory('cart', ['$rootScope',
        function($rootScope) {
            $rootScope.cart = {
                products: [],
                isHasProduct: function(product) {
                    var result = false
                    this.products.forEach(function(ele) {
                        if (ele.product == product) {
                            result = true
                            return
                        }
                    })
                    return result
                },

                addProduct: function(product) {
                    this.products.push({
                        product: product,
                        number: 0
                    })
                },

                updateProduct: function(product) {
                    this.products.forEach(function(ele) {
                        if (ele.product == product) {
                            ele.number += 1
                        }
                    })
                },

                empty: function() {
                    this.products = []
                },

                calculateTotalPrice: function() {
                    var totalPrice = 0
                    this.products.forEach(function(ele) {
                        totalPrice += ele.product.price * ele.number
                    })
                    return totalPrice
                }

                // remove: function(product){
                //     for(var i = 0 ; i <= this.products.length - 1 ; i++){
                //         if(this.products[i] == product){

                //         }
                //     }
                // }
            }

            return $rootScope.cart
        }
    ])
    .directive('daterangepicker', [
        function() {
            return {
                restrict: 'A',
                scope: {
                    date: '='
                },
                link: function($scope, ele) {
                    $(ele).html($scope.date.format('DD/MM/YYYY'))
                    $(ele).daterangepicker({
                        "singleDatePicker": true,
                        "autoApply": true,
                        startDate: moment(),
                        format: 'DD/MM/YYYY',
                        "opens": "left"
                    }, function(startDate, endDate) {
                        $scope.date = startDate
                        $(ele).html($scope.date.format('DD/MM/YYYY'))
                        $scope.$apply()
                    })
                }
            }
        }
    ])
