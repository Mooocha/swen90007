"use strict";

angular
    .module("app.components.customer.index", [])
    .controller("customerIndexCtrl", ["$scope", 'restapi', '$state', 'categories',
        function($scope, restapi, $state, categories) {
            var init = function() {
                restapi.sellers.get()
                    .success(function(res) {
                        $scope.sellerList = res.sellers;
                    })
            };

            init();
        }
    ]);
