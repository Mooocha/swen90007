"use strict";

angular
    .module("app.components.nav", [])
    .controller("navCtrl", ["$scope", "user", "$state", 'cart', 'restapi',
        function($scope, user, $state, cart, restapi) {
            $scope.name = user.get().firstName
            $scope.role = user.get().role
            $scope.logout = function() {
                user.remove()
                $state.go("app.shared.loginAndRegister", {}, {
                    reload: true
                })
            }

            $scope.cartClick = function() {
                $('#cartModal').modal()
            }

            $scope.deliverTime = moment()

            $scope.payClick = function() {
                if (cart.products.length == 0) {
                    return
                }
                restapi.orders.post({
                        sellerId: $state.params.id,
                        customerId: user.get().id,
                        status: 'paid',
                        products: cart.products.map(function(ele) {
                            return {
                                product: ele.product._id,
                                amount: ele.number
                            }
                        }),
                        price: cart.calculateTotalPrice(),
                        deliverTime: $scope.deliverTime
                    })
                    .success(function(res) {
                        cart.empty()
                        alert('payment succeed')
                        $('#cartModal').modal('hide')
                    })
            }
        }
    ]);
