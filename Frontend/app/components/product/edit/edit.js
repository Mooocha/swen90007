"use strict";

angular
    .module("app.components.product.edit", [])
    .controller("productEditCtrl", ["$scope", "restapi", "user", "Upload", "$stateParams", "$state", 'categories',
        function($scope, restapi, user, Upload, $stateParams, $state, categories) {
            var params = {
                "_id": $stateParams.id
            }
            $scope.flag = false

            $scope.product = ""
                //get all categories
            restapi.products.get()
                .success(function(res) {
                    $scope.categories = categories(res.products)
                })
                .error(function(res) {
                    console.log(res)
                })
                //get product detail
            restapi.product.get(params)
                .success(function(res) {
                    $scope.product = res.product
                })
                .error(function(res) {
                    console.log(res)
                })
                //edit product
            $scope.save = function() {
                console.log($scope.product)
                restapi.product.put($scope.product)
                    .success(function(res) {
                        console.log(res)
                        if (res.error) {
                            console.log(res.error)
                        } else {
                            alert("success")
                        }
                    })
                }
                //delete product
            $scope.delete = function() {
                restapi.product.delete($scope.product)
                    .success(function(res) {
                        if (res.msg) {
                            alert(res.msg)
                            $state.go("app.seller.index")
                        } else if (res.error) {
                            alert(res.error)
                        }
                    })
                    .error(function(res) {
                        console.log(res)
                    })
            }
        }
    ]);