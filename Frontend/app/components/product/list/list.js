"use strict";

angular
    .module("app.components.product.list", [])
    .controller("productListCtrl", ["$scope", 'restapi', '$state', '$stateParams', '$q', 'categories', 'cart',
        function($scope, restapi, $state, $stateParams, $q, categories, cart) {
            var init = function() {
                $scope.term = "";
                $q.all([
                    restapi.seller.get($stateParams.id),
                    restapi.products.get({
                        sellerId: $stateParams.id
                    })
                ]).then(function(value) {
                    $scope.seller = value[0].data.seller
                    $scope.productList = value[1].data.products
                    $scope.categories = categories($scope.productList)
                    $scope.categoryProducts = $scope.productList
                })

                cart.empty()
            };

            $scope.categoryClick = function(category) {
                if (category == "all") {
                    $scope.categoryProducts = $scope.productList
                    return
                }
                var products = []
                $scope.productList
                    .forEach(function(product) {
                        if (product.category == category) {
                            products.push(product)
                        }
                    })
                $scope.categoryProducts = products
            }

            init();

            $scope.buyClick = function(product) {
                if (!cart.isHasProduct(product)) {
                    cart.addProduct(product)
                } else {
                    cart.updateProduct(product)
                }
            }

            $scope.search = function(key) {
                if (key.which == 13) {
                    $scope.categoryProducts = $scope.productList
                    var searchArray = []
                    if ($scope.term != "") {
                        for (var i = 0; i < $scope.categoryProducts.length; i++) {
                            console.log($scope.categoryProducts.name)
                            if ($scope.categoryProducts[i].name.search($scope.term) != -1) {
                                searchArray.push($scope.categoryProducts[i])
                            }
                        }
                        $scope.categoryProducts = searchArray
                    }
                }
            };
        }
    ]);
