"use strict";

angular
    .module("app.components.product.new", [])
    .controller("productNewCtrl", ["$scope", "restapi", "user", "Upload", "categories",
        function($scope, restapi, user, Upload, categories) {
            $scope.flag = false
            $scope.product = {
                "name": "",
                "price": "",
                "imageUrl": "",
                "category": "",
                "state": "new",
                "rating": "0",
            }
            $scope.categories = []


             //get all categories
            restapi.products.get()
                .success(function(res) {
                    $scope.categories = categories(res.products)
                })
                .error(function(res) {
                    console.log(res)
                })

            $scope.save = function() {
                restapi.products.create($scope.product)
                    .success(function(res) {
                        alert(res.msg)
                    })
                    .error(function(res) {
                        console.log(res)
                    })
            }
        }
    ]);