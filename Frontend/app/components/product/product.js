"use strict"

angular
	.module("app.components.product", [
		"app.components.product.new",
		"app.components.product.edit",
		"app.components.product.list"
		])