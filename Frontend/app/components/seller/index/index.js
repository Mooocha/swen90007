"use strict";

angular
    .module("app.components.seller.index", [])
    .controller("sellerIndexCtrl", ["$scope", 'restapi', '$state', 'categories',
        function($scope, restapi, $state, categories) {
            restapi
                .products
                .get()
                .success(function(res) {
                    console.log(res)
                    $scope.products = res.products
                    $scope.categories = categories(res.products)
                    $scope.categoryProducts = $scope.products
                })
                .error(function(res){
                    console.log(res)
                })

            $scope.categoryClick = function(category) {
                if (category == "all") {
                    $scope.categoryProducts = $scope.products
                    return
                }
                var products = []
                $scope.products
                    .forEach(function(product) {
                        if (product.category == category) {
                            products.push(product)
                        }
                    })

                $scope.categoryProducts = products
            }
        }
    ]);
