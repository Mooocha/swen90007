"use strict";

angular
    .module("app.components.seller.manage", [])
    .controller("sellerManageCtrl", ["$scope", 'restapi', '$state', 'user', '$stateParams', '$q',
        function($scope, restapi, $state, user, $stateParams, $q) {
            $scope.products = []
            var https = []
            $scope.role = user.get().role

            var init = function() {
                restapi.order.get($stateParams.id)
                    .success(function(res) {
                        $scope.order = res.order
                        for (var i = 0; i < $scope.order.products.length; i++) {
                            https.push(restapi.product.get({
                                "_id": $scope.order.products[i].product
                            }))
                        }
                        $q.all(https).then(function(value) {
                            for (var i = 0; i < value.length; i++) {
                                $scope.products.push(value[i].data.product)
                            }

                            for (var j = 0; j < $scope.products.length; j++) {
                                for (var k = 0; k < $scope.order.products.length; k++) {
                                    if ($scope.products[j]._id == $scope.order.products[k].product) {
                                        $scope.products[j].amount = $scope.order.products[k].amount
                                    }
                                }
                            }
                            console.log($scope.products)
                        })
                    })
            }

            init();

            $scope.approve = function() {
                restapi.order.put($stateParams.id, {
                        "status": "canceled"
                    })
                    .success(function(res) {
                        $state.reload()
                        console.log(res)
                    })
            }
        }
    ]);