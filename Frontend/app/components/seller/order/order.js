"use strict";

angular
    .module("app.components.seller.order", [])
    .controller("sellerOrderCtrl", ["$scope", 'restapi', '$state', 'categories', 'user',
        function($scope, restapi, $state, categories, user) {
            var orderData = [];
            $scope.role = user.get().role

            var init = function() {
                restapi.orders.get()
                    .success(function(res) {
                        orderData = res.orders
                        $scope.orders = res.orders
                        console.log($scope.orders)
                    })
            }

            init()

            $scope.orders = orderData

            $scope.filter = function(status) {
                if (status == "all") {
                    $scope.orders = orderData
                    return
                }
                var orders = []
                orderData
                    .forEach(function(order) {
                        if (order.status == status) {
                            orders.push(order)
                        }
                    })

                $scope.orders = orders
            }

            $scope.approve = function(id) {
                restapi.order.put(id, {
                        "status": "canceled"
                    })
                    .success(function(res) {
                        $state.reload()
                        console.log(res)
                    })
            }

            $scope.cancel = function(id) {
                restapi.order.put(id, {
                        "status": "request to cancel"
                    })
                    .success(function(res) {
                        $state.reload()
                        console.log(res)
                        if (res.msg == 'Time less than 1 day!') {
                            alert('You cant cancel this order because it will be delivered within 1 day!')
                        }
                    })
            }
        }
    ]);
