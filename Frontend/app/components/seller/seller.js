"use strict"

angular
	.module("app.components.seller", [
		"app.components.seller.index",
		"app.components.seller.order",
		"app.components.seller.manage"
		])