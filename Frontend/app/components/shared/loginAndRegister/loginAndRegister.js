"use strict";

angular
    .module("app.components.shared.loginAndRegister", [])
    .controller("loginAndRegisterCtrl", ["$scope", "restapi", "$window", "user", "$state",
        function($scope, restapi, $window, user, $state) {
            $scope.loginUser = {
                "email": "",
                "password": ""
            }

            $scope.confirmation = ""

            $scope.registerUser = {
                    "email": "",
                    "password": "",
                    "lastName": "",
                    "firstName": ""
                }
                //if userType == true, then user type is customer, else is seller
            $scope.userType = true

            $scope.login = function() {
                user.remove()
                if ($scope.loginUser.email && $scope.loginUser.password) {
                    restapi.tokens.login($scope.loginUser)
                        .success(function(res) {
                            console.log(res)
                            if (res.token) {
                                user.set(res.id, res.role, res.firstName, res.token)
                                if (res.role == "customer") {
                                    $state.go("app.customer.index", {}, {reload: true})
                                } else {
                                    $state.go("app.seller.index", {}, {reload: true})
                                }
                            }

                        })
                        .error(function(res) {
                            console.log(res)
                        })
                } else {
                    alert("email and password cannot be empty!")
                }
            }

            $scope.register = function() {
                if ($scope.confirmation == $scope.registerUser.password) {
                    if ($scope.userType) {
                        restapi.customers.register($scope.registerUser)
                            .success(function(res) {
                                alert("Register successfully!")
                                $scope.registerUser = ""
                                $scope.confirmation = ""
                                console.log(res.customer)
                            })
                            .error(function(res) {
                                alert(res.msg)
                            })
                    } else {
                        restapi.sellers.register($scope.registerUser)
                            .success(function(res) {
                                alert("Register successfully!")
                                $scope.registerUser = ""
                                $scope.confirmation = ""
                                console.log(res.seller)
                            })
                            .error(function(res) {
                                alert(res.msg)
                            })
                    }
                } else {
                    alert("passwords do not match!")
                }
            }
        }
    ])