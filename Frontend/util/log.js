'use strict'

var moment = require('moment')

var log = {
    timeStamp: function() {
        return moment().format('ss:mm:HH, DD/MM/YYYY')
    },

    info: function(msg) {
        var infoLog = '[ Info ]' + '[ ' + this.timeStamp() + ' ] ' + msg
        console.log(infoLog)
    },

    error: function(msg) {
        var errorLog = '[ Error ]' + '[ ' + this.timeStamp() + ' ] ' + msg
        console.log(errorLog)
    },

    debug: function(msg) {
        if (process.env.NODE_ENV == 'development') {
            var debugLog = '[ Debug ]' + '[ ' + this.timeStamp() + ' ] ' + msg
            console.log(debugLog)
        }
    }
}

module.exports = log
